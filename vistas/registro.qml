import QtQuick 2.0
import QtQuick.Window 2.2
import QtQuick.Layouts 1.1

import Material 0.3
import Material.ListItems 0.1 as ListItem

import "qrc:/controladores/ControladorRegistro.js" as Controlador


Page
{
    id: registro
    actionBar.hidden: true
    width: Screen.width
    height: Screen.height


    Item
    {
        width: parent.width*0.8
        height: parent.height*0.6
        anchors.centerIn: parent
        z:2

        ColumnLayout
        {
            id: formularioLogin
            width: parent.width
            height: parent.height
            anchors.centerIn: parent

            TextField
            {
                id: nombre
                implicitWidth:  parent.width*0.7
                Layout.alignment: Qt.AlignHCenter
                placeholderText: qsTr("Usuario")
                floatingLabel: true
            }

            TextField
            {
                id: contrasena
                implicitWidth:  parent.width*0.7
                Layout.alignment: Qt.AlignHCenter
                placeholderText: qsTr("Contrasena")
                echoMode: TextInput.Password
                floatingLabel: true
            }

            TextField
            {
                id: cedula
                implicitWidth:  parent.width*0.7
                Layout.alignment: Qt.AlignHCenter
                placeholderText: qsTr("Cedula")
                floatingLabel: true
            }

            TextField
            {
                id: correo
                implicitWidth:  parent.width*0.7
                Layout.alignment: Qt.AlignHCenter
                placeholderText: qsTr("Correo")
                floatingLabel: true
            }

            TextField
            {
                id: telefono
                implicitWidth:  parent.width*0.7
                Layout.alignment: Qt.AlignHCenter
                placeholderText: qsTr("Telefono")
                floatingLabel: true
            }

            MenuField
            {
                id: genero
                model: [qsTr("Genero"), qsTr("Masculino"), qsTr("Femenino")]
                Layout.preferredHeight: registro.height*0.05
                Layout.preferredWidth: parent.width*0.5
                anchors.left: parent.left
                anchors.leftMargin: 60
            }

            Button
            {
                id:botonRegistro
                text: qsTr("Registrar")
                Layout.alignment: Qt.AlignHCenter
                backgroundColor: tema.primaryColor
                onClicked: Controlador.registrar()
            }
        }
    }

    Image
    {
        id: regresar
        z:2
        width: 50
        height: width
        source: "qrc:/icons/navigation/chevron_left.svg"

        MouseArea
        {
            anchors.fill: parent
            onClicked: pageStack.pop("qrc:/registro")
        }

        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.rightMargin: parent.width*0.02
        anchors.bottomMargin: parent.height*0.02
    }

    Image
    {
        id: fondo
        source: "qrc:/imagenes/fondo"
    }
}
