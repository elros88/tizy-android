import QtQuick 2.0
import QtQuick.Window 2.2
import QtQuick.Layouts 1.3

import Material 0.3
import Material.ListItems 0.1 as ListItem


import "qrc:/controladores/ControladorMenu.js" as Controlador

Page
{
    id: perfil
    width: parent.width
    height: parent.height

    title: qsTr("Perfil")

    ActionBar
    {
        customContent: Text {
            id: titulo
            text: qsTr("Perfil")
            color: tema.backgroundColor
            anchors.centerIn: parent
        }
    }



    ListModel
    {
        id: listaMenu

        ListElement
        {
            icono: "action/home"
            pagina: "Perfil"
            url: "qrc:/perfil"
        }
        ListElement
        {
            icono: "action/shopping_cart"
            pagina: "Saldo"
            url:"qrc:/saldo"
        }
        ListElement
        {
            icono: "action/store"
            pagina: "Qr"
            url:"qrc:/codigo"
        }
    }



    backAction: menu.action

    NavigationDrawer
    {
        id: menu

        Item
        {
            id: lista
            width: parent.width
            height: parent.height*0.7
            anchors.top: parent.top

            ListView
            {
                id: listaOpciones
                anchors.fill: parent
                model: listaMenu
                delegate: elementoMenu
                visible: true
            }

            Component
            {
                id: elementoMenu

                Item
                {
                    width: listaOpciones.width*0.9
                    height: listaOpciones.height*0.1
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.topMargin: dp(5)

                    Icon
                    {
                        id: iconoElemento
                        name: icono
                        anchors.verticalCenter: parent.verticalCenter
                    }

                    Text
                    {
                        text: pagina
                        font.pointSize: 14
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.left: iconoElemento.right
                        anchors.leftMargin: parent.width*0.05
                    }

                    ThinDivider
                    {
                        width: parent.width
                        anchors.top: iconoElemento.bottom
                    }

                    MouseArea
                    {
                        anchors.fill: parent
                        onClicked: Controlador.abrirPagina(url)
                    }
                }
            }
        }


    }



    ColumnLayout
    {
        width: parent.width
        height: parent.height*0.5
        anchors.centerIn: parent

        Text
        {
            id: nombre
            Layout.preferredWidth: parent.width*0.7
            Layout.alignment: Qt.AlignHCenter
            text: "Nombre: "+usuario.nombre
        }

        Text
        {
            id: cedula
            Layout.preferredWidth:  parent.width*0.7
            Layout.alignment: Qt.AlignHCenter
            text: "Cedula: "+usuario.cedula
        }

        Text
        {
            id: telefono
            Layout.preferredWidth:  parent.width*0.7
            Layout.alignment: Qt.AlignHCenter
            text: "Telefono: "+usuario.telefono
        }

        Text
        {
            id: correo
            Layout.preferredWidth:  parent.width*0.7
            Layout.alignment: Qt.AlignHCenter
            text: "Correo: "+usuario.correo
        }

        Text
        {
            id: genero
            Layout.preferredWidth:  parent.width*0.7
            Layout.alignment: Qt.AlignHCenter
            text: "Genero: " + (usuario.genero == 1 ? "Masculino" : "Femenino")
        }

    }
}

