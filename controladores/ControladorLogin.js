function login()
{
    usuario.cedula = cedula.text;
    usuario.contrasena = contrasena.text;
    restLogin.post(usuario.crearJSONLogin())
}

function recibirRespuesta()
{
    if(restLogin.validarRespuesta())
    {
        usuario.parsearJSON(restLogin.getObjetoJson());
        pageStack.push("qrc:/perfil");
    }

}
