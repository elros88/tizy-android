#include "restful.h"

Rest *Restful::getEndpointRegistro() const
{
    return endpointRegistro;
}

void Restful::setEndpointRegistro(Rest *value)
{
    endpointRegistro = value;
}

Rest *Restful::getEndpointLogin() const
{
    return endpointLogin;
}

void Restful::setEndpointLogin(Rest *value)
{
    endpointLogin = value;
}

Rest *Restful::getEndpointPerfil() const
{
    return endpointPerfil;
}

void Restful::setEndpointPerfil(Rest *value)
{
    endpointPerfil = value;
}

Rest *Restful::getEndpointQr() const
{
    return endpointQr;
}

void Restful::setEndpointQr(Rest *value)
{
    endpointQr = value;
}

Rest *Restful::getEndpointBalance() const
{
    return endpointBalance;
}

void Restful::setEndpointBalance(Rest *value)
{
    endpointBalance = value;
}

Rest *Restful::getEndpointPago() const
{
    return endpointPago;
}

void Restful::setEndpointPago(Rest *value)
{
    endpointPago = value;
}

Restful::Restful(QObject *parent) : QObject(parent)
{
    endpointLogin = new Rest(this);
    endpointLogin->iniciar(direcciones.login);
    endpointRegistro= new Rest(this);
    endpointRegistro->iniciar(direcciones.registro);
    endpointQr = new Rest(this);
    endpointQr->iniciar(direcciones.qr);
    endpointBalance = new Rest(this);
    endpointBalance->iniciar(direcciones.balance);
    endpointPago = new Rest(this);

}
