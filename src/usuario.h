#ifndef USUARIO_H
#define USUARIO_H

#include <QObject>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QByteArray>

class Usuario : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString nombre READ getNombre WRITE setNombre NOTIFY nombreCambio)
    Q_PROPERTY(QString contrasena READ getContrasena WRITE setContrasena NOTIFY nombreCambio)
    Q_PROPERTY(QString cedula READ getCedula WRITE setCedula NOTIFY cedulaCambio)
    Q_PROPERTY(QString telefono READ getTelefono WRITE setTelefono NOTIFY nombreCambio)
    Q_PROPERTY(QString correo READ getCorreo WRITE setCorreo NOTIFY correoCambio)
    Q_PROPERTY(int genero READ getGenero WRITE setGenero NOTIFY generoCambio)
    Q_PROPERTY(double saldo READ getSaldo WRITE setSaldo NOTIFY saldoCambio)
    Q_PROPERTY(QString token READ getToken WRITE setToken NOTIFY tokenCambio)

    QString nombre;
    QString contrasena;
    QString cedula;
    QString correo;
    QString telefono;
    int genero;
    QString token;
    double saldo;

public:
    explicit Usuario(QObject *parent = nullptr);

    QString getNombre() const;
    void setNombre(const QString &value);

    QString getContrasena() const;
    void setContrasena(const QString &value);

    QString getCedula() const;
    void setCedula(const QString &value);

    QString getCorreo() const;
    void setCorreo(const QString &value);

    QString getTelefono() const;
    void setTelefono(const QString &value);

    int getGenero() const;
    void setGenero(const int &value);

    Q_INVOKABLE void parsearJSON(QJsonObject respuesta);
    Q_INVOKABLE QByteArray crearJSONLogin();
    Q_INVOKABLE QByteArray crearJSONRegistro();
    Q_INVOKABLE  void parsearSaldo(QJsonObject respuesta);

    QString getToken() const;
    void setToken(const QString &value);

    double getSaldo() const;
    void setSaldo(double value);

signals:

    void nombreCambio();
    void contrasenaCambio();
    void cedulaCambio();
    void correoCambio();
    void telefonoCambio();
    void generoCambio();
    void tokenCambio();
    void saldoCambio();



public slots:
};

#endif // USUARIO_H
