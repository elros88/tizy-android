#ifndef RESTFUL_H
#define RESTFUL_H

#include <QObject>
#include <rest.h>
#include <src/direcciones.h>

class Restful : public QObject
{
    Q_OBJECT

    Rest* endpointRegistro;
    Rest* endpointLogin;
    Rest* endpointPerfil;
    Rest* endpointQr;
    Rest* endpointBalance;
    Rest* endpointPago;
    Direcciones direcciones;


public:
    explicit Restful(QObject *parent = nullptr);

    Rest *getEndpointRegistro() const;
    void setEndpointRegistro(Rest *value);

    Rest *getEndpointLogin() const;
    void setEndpointLogin(Rest *value);

    Rest *getEndpointPerfil() const;
    void setEndpointPerfil(Rest *value);

    Rest *getEndpointQr() const;
    void setEndpointQr(Rest *value);

    Rest *getEndpointBalance() const;
    void setEndpointBalance(Rest *value);

    Rest *getEndpointPago() const;
    void setEndpointPago(Rest *value);

signals:

public slots:
};

#endif // RESTFUL_H
